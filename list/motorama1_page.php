<!DOCTYPE HTML>
<html lang="en">
    <head>
        <title>Vinyl World - магазин виниловых пластинок</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="stylesheet" href="../style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon">
   </head>
	<body>
	
		<header >
			<a href="../index.html"><img src="../img/logo.png" alt="logo" class="logo_img"></a>
			<a href="../index.html"><div>
				<p class="logo">Vinyl World</p>
				<p class="description">Магазин виниловых пластинок</p>
			</div>	</a>

			<ul>
				<li class="menu_adress"><p>+7(999)128-59-65 г. Москва<p></li>
				<li><a href="../index.html">Главная</a></li>
				<li><a href="../list.html" class='activelink'>Каталог</a></li>
				<li><a href="../howwork.html">Как заказать?</a></li>
				<li><a href="../about.html">О нас</a></li>
				<li><a href="../contact.html">Контакты</a></li>
			</ul>
			

			<div class="header2">
				<ul class="nav menu">
						
					<li class="nav-item">
						<img class="nav-link" href=" " src="../img/menu.png" alt>
						<ul class="submenu">
							<li><a href="../index.html">Главная</a></li>
							<li><a href="../list.html">Каталог</li>
							<li><a href="../howwork.html">Как заказать?</a></li>
							<li><a href="../about.html">О нас</a></li>
							<li><a href="../contact.html">Контакты</a></li>
						</ul>
					</li>	

				</ul>
			</div>
			
			
			
		</header>
		<div class="header1">
		</div>
		
		
		
		<div class="item_page">
			<img src="../img/covers/Motorama 'Alps' 2010.png" alt>
			<div>
				<h1>Motorama</h1>
				<p><b>Альбом:</b> Alps</p>
				<p><b>Год издания:</b> 2010</p>
				<p><b>Страна:</b> Россия</p>
				<p><b>Жанр:</b> Рок, Поп-музыка</p>
				<p class='price'>Стоимость: 2000 руб.</p>
				<!--<a href="../writeus.html"><button type="button" class="btn">Оставить заявку!</button></a>-->
			</div>
		</div>
		
		<div class='backtolist'>
			<a href="../list.html"><button type="button" class="btn">Назад в каталог</button></a>
		</div>
		<div class="feedback_div" style="width: 100%; max-width:600px; margin: 0 auto;">
		<h3 style="	text-align: center; font-family: text_font;">Отзывы</h3>
		<?php
			$page_id = 5;
			
			$host = 'std-mysql'; 
			$database = 'std_234'; 
			$user = 'std_234'; 
			$password = 'popygai18'; 
		
			$link = mysqli_connect($host, $user, $password, $database) or die("Ошибка " . mysqli_error($link));
			
			$query = "SELECT * FROM Feedback WHERE Page_ID = 5";

			if ($result = $link->query($query)) {
				while ($row = $result->fetch_assoc()) {
					echo '<div style=" width: 100%; max-width: 600px;  font-family: text_font; margin-bottom: 20px; background-color: #f3d5d2; border: 3px  dotted #c5c5c5;">';
					echo '<div style="display: inline-block; width: 100%; max-width: 150px; padding-left: 20px;">';
					echo "<p style='font-size: 20pt;'>";
					echo $row['Name'];
					echo "</p>";
					echo "<p>";
					echo $row['Email'];
					echo "</p>";
					echo '</div>';
					echo '<div style="display: inline-block; vertical-align: top; width: 100%; max-width: 400px;  padding-left: 20px;">';
					echo $row['Message'];
					echo '</div>';
					echo '</div>';
				}
				$result->free();
			}
			
		?>
		</div>
		<form class="feedback_form"  enctype="multipart/form-data" method="post" action="comments.php">
				<h3>Оставить отзыв</h3>
				<div>
						<label for="name_f">Имя</label>
						<input name="name" type="text" id="name_f" placeholder="Введите ваше имя">
					</div>
					
					<div>
						<label for="email_f">Email</label>
						<input name="email" type="email" id="email_f" placeholder="Введите ваш email">
					</div>
					
					
					<div>
						<label for="message_f">Сообщение</label>
						
						<textarea name="message" id="message_f" rows="5"></textarea>
					</div>
				
				<div class='d-flex justify-content-center'>
					<div class="btn_feedback d-flex justify-content-end">
						<input type="hidden" name="page_id" value="5" />
						<button type="submit" class="btn">Отправить</button>
					</div>
				</div>
		</form>
		
		
		
		
		<footer class="footer1">
			<div class="footer_content d-flex justify-content-center">
					<div>
						<ul>
							<li><a href="../index.html">Главная</a></li>
							<li><a href="../list.html">Каталог</a></li>
							<li><a href="../howwork.html">Как заказать?</a></li>
							<li><a href="../about.html">О нас</a></li>
							<li><a href="../contact.html">Контакты</a></li>
						</ul>
					</div>

					<div>
						<p>Остались вопросы?</p>
						<p>+7(999)128-59-65</p>
						<a href="" ><img src="../img/social/inst.png" alt></a>
						<a href="" ><img src="../img/social/twitter.png" alt></a>
						<a href="" ><img src="../img/social/facebook.png" alt></a>
					</div>
					
			</div>
			<div class="d-flex justify-content-center">
				<p class="copyright">2018 Vinyl World. Все права защищены.</p>
			</div>
		</footer>
		
		<div class="footer2">
		
			<div>
				<p>Остались вопросы?</p>
				<p>+7(999)128-59-65</p>
				<a href="" ><img src="../img/social/inst.png" alt></a>
				<a href="" ><img src="../img/social/twitter.png" alt></a>
				<a href="" ><img src="../img/social/facebook.png" alt></a>
			</div>
		
			<div class="d-flex justify-content-center">
				<p class="copyright">2018 Vinyl World. Все права защищены.</p>
			</div>
		</div>
		
		
		
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	</body>
</html>	