<!DOCTYPE HTML>
<html lang="en">
    <head>
		<title>Vinyl World - магазин виниловых пластинок</title>
        <meta charset="utf-8">
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
	</head>
	<body>
			<?php
				require 'class_phpmailer.php';
				require 'class_smtp.php';

				$name = $_POST['name'];
				$number = $_POST['number'];
				$email = $_POST['email'];
				$message = $_POST['message'];

				$mail = new PHPMailer;

				$mail->isSMTP(); 
				$mail->Host = 'smtp.yandex.ru'; 
				$mail->SMTPAuth = true; 
				$mail->Username = 'vinylworld';
				$mail->Password = 'vinylworldproject';
				$mail->SMTPSecure = 'ssl'; 
				$mail->Port = 465;
				$mail->CharSet="UTF-8";
				$mail->setFrom('vinylworld@yandex.ru');
				$mail->addAddress('vinylworld@yandex.ru');

				$out_text = "Имя: ".$name.". Email: ".$email.". Номер телефона: ".$number.". Сообщение: ".$message;
				$mail->isHTML(true);
				$mail->Subject = "Новая заявка на сайте Vinyl World";
				$mail->Body = "$out_text";

				if(!$mail->send()) {
					echo '<h1 style="text-align: center; font-family: text_font; margin-top: 20px;">Сообщение не может быть отправлено</h1>';
					echo 'Ошибка: ' . $mail->ErrorInfo;
				}
				else {
					echo '<h1 style="text-align: center; font-family: text_font; margin-top: 20px;">Ваша заявка успешно отправлена!</h1>';
				}
					
			?>
			
			<div class="d-flex justify-content-center">
				<a href="index.html" style="font-family: text_font; margin-top: 20px; text-align: center; color: black; text-decoration: none;"><p style="width: 200px; padding:10px; background-color: #ff7bac; border-radius: 8px;">Вернуться на главную</p></a>
			</div>
</body>
</html>