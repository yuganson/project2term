<!DOCTYPE HTML>
<html lang="en">
    <head>
		<title>Vinyl World - магазин виниловых пластинок</title>
        <meta charset="utf-8">
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
	</head>
	<body>
			<?php
				require 'class_phpmailer.php';
				require 'class_smtp.php';

				$name = $_POST['name_q'];
				$email = $_POST['email_q'];
				$message = $_POST['message_q'];

				$mail = new PHPMailer;

				$mail->isSMTP(); 
				$mail->Host = 'smtp.yandex.ru'; 
				$mail->SMTPAuth = true; 
				$mail->Username = 'vinylworld';
				$mail->Password = 'vinylworldproject';
				$mail->SMTPSecure = 'ssl'; 
				$mail->Port = 465;
				$mail->CharSet="UTF-8";
				$mail->setFrom('vinylworld@yandex.ru');
				$mail->addAddress('vinylworld@yandex.ru');

				$out_text = "Имя: ".$name.". Email: ".$email.". Сообщение: ".$message;
				$mail->isHTML(true);
				$mail->Subject = "Новый вопрос на сайте Vinyl World";
				$mail->Body = "$out_text";

				if(!$mail->send()) {
					echo '<h1 style="text-align: center; font-family: text_font; margin-top: 20px;">Вопрос не может быть отправлен</h1>';
					echo 'Ошибка: ' . $mail->ErrorInfo;
				}
				else {
					echo '<h1 style="text-align: center; font-family: text_font; margin-top: 20px;">Ваш вопрос успешно отправлен! Ждите ответа на указанный email.</h1>';
				}
					
			?>
			
			<div class="d-flex justify-content-center">
				<a href="index.html" style="font-family: text_font; margin-top: 20px; text-align: center; color: black; text-decoration: none;"><p style="width: 200px; padding:10px; background-color: #ff7bac; border-radius: 8px;">Вернуться на главную</p></a>
			</div>
</body>
</html>